#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <DNSServer.h>
#include <DHT.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>
#include <ArduinoOTA.h>

const char* MQTT_SERVER = "192.168.0.2";
const char* MQTT_USER = "homeassistant";
const char* MQTT_PASSWORD = "2032";
const String ROOM = "den";
const boolean MQTT_LOG = false;

const bool MOVEMENT_ENABLED = false;
const bool TEMP_HUMIDITY_ENABLED = true;

const String CLIENT_ID = ROOM + "_controller";
const String MOVEMENT_TOPIC = "sensor/" + ROOM + "/movement";
const String HUMIDITY_TOPIC = "sensor/" + ROOM + "/humidity";
const String TEMPERATURE_TOPIC = "sensor/" + ROOM + "/temperature";

const uint8_t SENSOR_PIN = D7;
const uint8_t DHT_TYPE = DHT11;
const uint8_t DHT_PIN = 5;

WiFiClient espClient;
DHT dht(DHT_PIN, DHT_TYPE, 11);
PubSubClient client(espClient);

void setup() {
  Serial.begin(115200);
  if (MOVEMENT_ENABLED) pinMode(SENSOR_PIN, INPUT);
  if (TEMP_HUMIDITY_ENABLED) dht.begin();

  setup_wifi();
  client.setServer(MQTT_SERVER, 1883);
  setup_OTA();
}

void setup_OTA() {
  ArduinoOTA.setHostname(CLIENT_ID.c_str());
  ArduinoOTA.onStart([]() {
    Serial.println("Start");
    mqtt_logger("INFO", "Starting ArduinoOTA");
  });

  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
    mqtt_logger("INFO", "Ending ArduinoOTA");
  });

  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });

  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });

  ArduinoOTA.begin();
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
//  mqtt_logger("INFO", "Client ID: " + String(CLIENT_ID) + " || IP Address: " + WiFi.localIP());
}

void setup_wifi() {
  WiFiManager wifiManager;
  wifiManager.autoConnect(CLIENT_ID.c_str());
}

void publish_message(String topic, String value) {
  Serial.print("Publishing message with value: ");
  Serial.print(value.c_str());
  Serial.print(" to topic: ");
  Serial.println(topic);
  client.publish(topic.c_str(), value.c_str(), true);
//  mqtt_logger("INFO", "Publishing message with value: " + value + " to topic: " + topic);
}

void mqtt_logger(String logLevel, String message) {
  if (MQTT_LOG) {
    String logTopic = "log/" + ROOM + "/" + logLevel;
    publish_message(logTopic, message);
  }
}

bool connected = false;
void reconnect() {
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection... ");
    if (client.connect(CLIENT_ID.c_str(), MQTT_USER, MQTT_PASSWORD)) {
      Serial.println("connected");
      connected = true;
    } else {
      Serial.print("Failed to connect, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

bool checkBoundFl(float newValue, float prevValue, float maxDiff) {
  return !isnan(newValue) &&
         (newValue < prevValue - maxDiff || newValue > prevValue + maxDiff);
}

bool checkBound(long newValue, long prevValue) {
  return !isnan(newValue) &&
         (newValue < prevValue || newValue > prevValue);
}

long roomLastMsg = 0;
float roomTemp = 0.0;
float roomHum = 0.0;
float roomDiff = 1.0;
void read_temp_humidity() {
  
  long now = millis();
  if (now - roomLastMsg > 2000) {
    roomLastMsg = now;
    Serial.println("Taking Room Humidity and Temperature");
    mqtt_logger("INFO", "Taking Room Humidity and Temperature");

    float newTemp = dht.readTemperature();
    float newHum = dht.readHumidity();

    if (checkBoundFl(newTemp, roomTemp, roomDiff)) {
      roomTemp = newTemp;
      Serial.print("New room temperature: ");
      Serial.println(String(roomTemp).c_str());
      mqtt_logger("INFO", "New room temperature: " + String(roomTemp));
      int publishTemp = roomTemp;
      publish_message(TEMPERATURE_TOPIC, String(publishTemp));
    } else {
      Serial.println("Room Temperature unchanged");
      mqtt_logger("INFO", "Room Temperature unchanged");
    }

    if (checkBoundFl(newHum, roomHum, roomDiff)) {
      roomHum = newHum;
      Serial.print("New room humidity: ");
      Serial.println(String(roomHum).c_str());
      mqtt_logger("INFO", "New room humidity: " + String(roomHum));
      int publishHum = roomHum;
      publish_message(HUMIDITY_TOPIC, String(publishHum));
    } else {
      Serial.println("Room Humidity unchanged");
      mqtt_logger("INFO", "Room Humidity unchanged");
    }
  }
}

long lastMovementMsg = 0;
long lastMovementReading = 0;
void read_movement() {
  long now = millis();
  if(now - lastMovementMsg > 1000) {
    lastMovementMsg = now;
    long newMovementReading = digitalRead(SENSOR_PIN);
    if (checkBound(newMovementReading, lastMovementReading)) {
      lastMovementReading = newMovementReading;
      if(newMovementReading == HIGH) {
        Serial.print("Motion detected! Status: ");
        Serial.println(String(newMovementReading));
        mqtt_logger("INFO", "Motion detected! Status: " + String(newMovementReading));
      } else {
        Serial.print("Motion absent! Status: ");
        Serial.println(String(newMovementReading));
        mqtt_logger("INFO", "Motion absent! Status: " + String(newMovementReading));
      }
      publish_message(MOVEMENT_TOPIC, String(newMovementReading));
    } else {
      Serial.print("Movement unchanged. Current status: ");
      Serial.println(lastMovementReading);
      mqtt_logger("INFO", "Motion unchanged! Current status: " + String(newMovementReading));
    }
  }
}

void loop() {

  ArduinoOTA.handle();
  if (!client.connected()) {
    connected = false;
    Serial.print("Not connected to MQTT. State: ");
    Serial.println(client.state());
    reconnect();
  }
  client.loop();

  if (MOVEMENT_ENABLED) read_movement();  
  if (TEMP_HUMIDITY_ENABLED) read_temp_humidity();
}
